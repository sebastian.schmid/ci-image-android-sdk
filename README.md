# Readme

This docker image is based on the [official openjdk image](https://hub.docker.com/_/openjdk) and includes:
- android-sdk

## Example

Using gitlab-ci.yml:
```yml
image: registry.gitlab.com/android-ci-docker-images/android-sdk:latest

stages:
  - build
  - test

lintDebug:
  stage: build
  script:
    - ./gradlew -Pci --console=plain :app:lintDebug -PbuildDir=lint

assembleDebug:
  stage: build
  script:
    - ./gradlew assembleDebug
  artifacts:
    paths:
    - app/build/outputs/

debugTests:
  stage: test
  script:
    - ./gradlew -Pci --console=plain :app:testDebug

unitTests:
  stage: test
  script:
    - ./gradlew test
```