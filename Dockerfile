# using official openjdk image
FROM openjdk:8-jdk-slim

LABEL maintainer="mail@sebastian-schmid.de"
LABEL Description="android-sdk-image"

# android tool versions
ARG ANDROID_COMPILE_SDK=28
ARG ANDROID_BUILD_TOOLS=28.0.2
ARG ANDROID_SDK_TOOLS=4333796

ENV DEBIAN_FRONTEND noninteractive

# install tools required by android-sdk
RUN apt-get --quiet update --yes \
    && apt-get --quiet install --yes --no-install-recommends \
       wget \
       tar \
       unzip \
       lib32stdc++6 \
       lib32z1 \
# install android-sdk
    && wget --quiet https://dl.google.com/android/repository/sdk-tools-linux-$ANDROID_SDK_TOOLS.zip -P /tmp \
    && unzip -d /opt/android-sdk /tmp/sdk-tools-linux-$ANDROID_SDK_TOOLS.zip \
    && echo y | /opt/android-sdk/tools/bin/sdkmanager "platforms;android-$ANDROID_COMPILE_SDK" >/dev/null \
    && echo y | /opt/android-sdk/tools/bin/sdkmanager "platform-tools" >/dev/null \
    && echo y | /opt/android-sdk/tools/bin/sdkmanager "build-tools;$ANDROID_BUILD_TOOLS" >/dev/null \
# clean up to keep image as little as posible
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/*

# set env vars for android-sdk
ENV ANDROID_HOME=/opt/android-sdk/
ENV PATH=$PATH:/opt/android-sdk/platform-tools/

# accept android-sdk licenses
RUN yes | /opt/android-sdk/tools/bin/sdkmanager --licenses
